<?php

$conn = new PDO("127.0.0.1", "root", "", "weJump");
 
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
 
mysqli_set_charset($conn, 'utf8');
 

$command = "	
CREATE DATABASE weJump

CREATE TABLE IF NOT EXISTS Produto
(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Nome VARCHAR(60) NOT NULL,
    SKU INTEGER,
    Preco VARCHAR(25),
    Descricao TEXT,
    Quantidade INTEGER,
    CATEGORY FOREIGN KEY INTEGER  
)

CREATE TABLE IF NOT EXISTS Category
(
	Id INT PRIMARY KEY AUTO_INCREMENT,
    Nome VARCHAR(60) NOT NULL
)

";
$sth = $conn->prepare($command);
$sth->execute();
$result = $sth->FETCH(PDO::FETCH_ASSOC);
?>