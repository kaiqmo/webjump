<?php

require_once '../config.php';

if(isset($_POST))
{
    
    if(empty($_POST["code"]))
        echo "Cade o sSkuuuu";
	if(empty($_POST["name"]))
        echo "Campo nome obrigatório";
    
    $code = intval($_POST['code']);

// on duplicate key update ja garante o update do mesmo item e nao precisa criar um novo metodo so para dar update no item.
    $command = "INSERT INTO category (id,name) values( :code, :name) ON DUPLICATE KEY UPDATE id=:code, name=:name" ;
    $sth = $conn->prepare($command);
    $sth->bindParam(':code', $code, PDO::PARAM_INT);
    $sth->bindParam(':name',$_POST['name'],  PDO::PARAM_STR);
    $res = $sth->execute();

    die();

// o uso de bindParam é feito para que não haja a possibilidade de ocorrer SQL inject

}else{
    echo "Error with your request";
    die();
}

?>