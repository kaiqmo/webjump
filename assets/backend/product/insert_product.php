<?php


require_once '../config.php';

    if(isset($_POST)){  
        //var_dump($_POST);
        //nome;sku;descricao;quantidade;preco;categoria
        //INSERT INTO product (name,sku,description,quantity,price,category) 
        //values ('Eplerenone',42254-011,'Bypass Innominate Artery to Bilateral Upper Leg Artery with Synthetic Substitute','Open Approach',59,2300.39,'Comedy|Horror|Thriller')
        if(empty($_POST["sku"]))
            echo "Cade o sSkuuuu";
        if(empty($_POST["name"]))
            echo "Campo nome obrigatório";
        if(empty($_POST["price"]))
            echo " Nada nessa vida é de graça. Coloque um preço";
        if(empty($_POST["quantity"]))
            echo "Queremos escala.";
        if(empty($_POST['category']))
            echo "Cade a categoria";
        if(empty($_POST['description']))
            $description = '';
        else 
            $description = $_POST['description'];

        $sku = intval($_POST['sku']);
        $category = $_POST['category'];        
        $quantity = intval($_POST['quantity']);
        $price = $_POST['price'];
        // on duplicate key upadte ja garante o update do mesmo item e nao precisa criar um novo metodo so para dar update no item.
        $command = "INSERT INTO product (name,sku,description,quantity,price,category) values( :name, :sku, :description, :quantity,:price,:category) ON DUPLICATE KEY UPDATE    
        sku=:sku,name=:name price=:price, quantity=:quantity, category=:category, description=:description ";
        $sth = $conn->prepare($command);
        $sth->bindParam(':sku', $sku, PDO::PARAM_INT);
        $sth->bindParam(':name',$_POST['name'], PDO::PARAM_STR);
        $sth->bindParam(':price', $price , PDO::PARAM_INT);
        $sth->bindParam(':quantity', $quantity, PDO::PARAM_INT);
        $sth->bindParam(':category', $category, PDO::PARAM_STR);
        $sth->bindParam(':description',$description, PDO::PARAM_STR);
        $res = $sth->execute();

        die();
    }
    else {
        echo 'Erro ao salvar configurações. Tente novamente mais tarde. Se o problema persistir entre em contato pelo email';
        die();
    }
?>