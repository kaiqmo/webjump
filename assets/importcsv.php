<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <title>readContactCSV.php</title>
</head>
<body>
 <h1>Contacts</h1>
 <div>
 <?php
  require_once './backend/config.php';
  print <<< HERE
  <table border = "1">
  <tr>
   <th>Name</th>
   <th>SKU</th>
   <th>Description</th>
   <th>Quantity</th>
   <th>Price</th>
   <th>Category</th>
  </tr>
  
  //nome;sku;descricao;quantidade;preco;categoria
HERE;
  $data = file("import.csv");
  foreach ($data as $line){
  $lineArray = explode(";", $line);
  list($fName, $Sku, $desc, $qtd, $price, $cat) = $lineArray;
  $command = "INSERT INTO product (name,sku,description,quantity,price,category) values( :name, :sku, :description, :quantity, :price, :category) ON DUPLICATE KEY UPDATE    
        sku=:sku,name=:name, price=:price, quantity=:quantity, category=:category, description=:description ";
        $sth = $conn->prepare($command);
        $sth->bindParam(':sku', $Sku);
        $sth->bindParam(':name',$fName);
        $sth->bindParam(':price', $price);
        $sth->bindParam(':quantity', $qtd);
        $sth->bindParam(':category', $cat);
        $sth->bindParam(':description',$desc);
        $res = $sth->execute();
  print <<< HERE
   <tr>
   <td>$fName</td>
   <td>$Sku</td>
   <td>$desc</td>
   <td>$qtd</td>
   <td>$price</td>
   <td>$cat</td>
   </tr>
HERE;
  } // end foreach
  //print the bottom of the table
  print "</table> n";
 ?>
 </div>
</body>
</html>